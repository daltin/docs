# Install Proxmox VE with ZFS root on Hetzner root server

- Save network data:
  - if name
  - ip
  - netmask
  - gw
  - ns list

- Boot to recovery

- exec:
```
apt update
apt install linux-headers-`uname -r`
apt install -y -t buster-backports dkms spl-dkms
apt install -y -t buster-backports zfs-dkms zfsutils-linux
wget -O /proxmox.iso "https://proxmox.com/en/downloads?task=callelement&format=raw&item_id=513&element=f85c494b-2b32-4109-b8c1-083cca2b7db6&method=download&args[0]=3dd7f9fad54bbbf4acce1adfe06c7833"

qemu-system-x86_64 -enable-kvm -m 8192 -drive file=/dev/sda,if=virtio,format=raw -drive file=/dev/sdb,if=virtio,format=raw -drive file=/dev/sdc,if=virtio,format=raw -drive file=/dev/sdd,if=virtio,format=raw -cdrom /proxmox.iso -boot d -vnc :0
```

- Connect by VNC to server IP, port 5900
- Install Proxmox with ZFS
- Stop qemu
- Run qemu from zfs:
```
qemu-system-x86_64 -enable-kvm -m 8192 -drive file=/dev/sda,if=virtio,format=raw -drive file=/dev/sdb,if=virtio,format=raw -drive file=/dev/sdc,if=virtio,format=raw -drive file=/dev/sdd,if=virtio,format=raw -vnc :0

```
- edit `/etc/network/interfaces` - add all variants of interface (include previous stored) :
```
bridge_ports ens3 eth0 eth1 enp2s0 enp3s0 eno1
```
- reboot server normally
